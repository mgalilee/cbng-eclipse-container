# CBNG + Eclipse container Dockerfile

## Purpose

The purpose of this container image is to provide a standardized, 'accco' compliant development environment for Java projects, that can be run on any Linux machine (e.g. GPN office computer, sshuttle'd home computer).

The image provides:
* CBNG
* JDK 8 and JDK 11
* X11 and GTK

Some resources are meant to be loaded from the host into the container, so as to be persisted beyond the lifetime of the container:
* Gradle/CBNG cache
* the actual Eclipse installation
* the Eclipse workspace
* the Java projects themselves

## Building

`docker build --build-arg HOST_UID=$UID --build-arg HOST_USER=$USER --network="host" -f Dockerfile -t cbng-eclipse .`

## Running

Examples given below are based on the case of the `pm-pic-ipoc-analysis` project and should be adapted.

### Preliminary steps

#### Installing Eclipse

Install Eclipse on your local machine, from e.g. `https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2021-09/R/eclipse-java-2021-09-R-linux-gtk-x86_64.tar.gz`

Eclipse could be part of the container, but this makes persistent installation of plugins complicated at best.

#### Creating a persistent Gradle/CBNG cache

`mkdir -p gradle_user_home`

actually required only once ever, can and should be re-used for all projects environments.

#### Checking out the project

`git clone https://gitlab.cern.ch/acc-co/pm/analysis/pm-pic-ipoc-analysis.git`

#### Creating a persistent Eclipse workspace for the project

`mkdir -p eclipse_workspaces/pm-pic-ipoc-analysis`

### Starting Eclipse

Adapting the paths to your own setup and project:

`docker run -it -v $(pwd)/eclipse/2021-09:/home/$USER/eclipse -v $(pwd)/pm-pic-ipoc-analysis:/home/$USER/workspace -v $(pwd)/eclipse_workspaces/pm-pic-ipoc-analysis:/home/$USER/eclipse-workspace -v $(pwd)/gradle_user_home:/home/$USER/gradle_user_home -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY -h $HOSTNAME -v $HOME/.Xauthority:/home/$USER/.Xauthority --network=host --userns=keep-id --dns-search=cern.ch cbng-eclipse:latest`

Note: `--userns=keep-id` is a Podman only arg, not needed if actual Docker is used.

Note: The hostname inside the container is set via the flag `-h` and can be changed at convenience, e.g. to `cmw-private-directory`.

Note: X11 related errors e.g. `Authorization required, but no authorization protocol specified` can be circumvented by authorizing explicitly the user on the host's X server: `xhost si:localuser:$USER`

### Usage

* From a terminal (e.g. within Eclipse or `exec`ing a shell on the running container), `cd ~/workspace && bob eclipse` (possibly with `bob` args `-PresolveEmptyAs=CTX` and/or `--refresh-dependencies`)
* Import freshly created project(s) into the Eclipse workspace
* Code happily
