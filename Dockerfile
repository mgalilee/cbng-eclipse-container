from gitlab-registry.cern.ch/acc-co/devops/docker/cbng-images:latest-jdk11

RUN yum -y install yum-plugin-ovl
RUN yum -y install xorg-x11-server-Xorg xorg-x11-xauth xorg-x11-apps xorg-x11-fonts-* \
giflib \
gtk3 \
dejavu* \
iputils \
krb5-workstation \
java-11-openjdk-src \
vim \
webkit* \
&& yum -y clean all \
&& rm -fr /var/cache

RUN wget http://linux.web.cern.ch/linux/docs/krb5.conf -O /etc/krb5.conf

ARG HOST_USER
ENV HOST_USER=$HOST_USER
ARG HOST_UID
ENV HOST_UID=$HOST_UID

RUN useradd -u $HOST_UID $HOST_USER && \
echo "$HOST_USER"':cern' | chpasswd && \
chown -R ${HOST_USER}:${HOST_USER} /home/$HOST_USER/ && \
mkdir -p /local/java.users/${HOST_USER} && \
chown ${HOST_USER}:${HOST_USER} /local/java.users/${HOST_USER}

USER $HOST_UID

ENV USER $HOST_USER
ENV GRADLE_USER_HOME /home/$HOST_USER/gradle_user_home
ENV BOB_USER_CACHE /home/$HOST_USER/gradle_user_home

RUN git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative"

CMD /home/$HOST_USER/eclipse/eclipse -data /home/$HOST_USER/eclipse-workspace
